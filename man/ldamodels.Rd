% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/ldamodels.R
\docType{data}
\name{ldamodels}
\alias{ldamodels}
\title{LDA models for inversion classification}
\format{List of LDA models for 6 inversions (ROIno.2.13, ROIno.3.2, ROIno.7.10, ROIno.8.3,
ROIno.16.13 and ROIno.17.16)}
\usage{
ldamodels
}
\description{
This dataset contains the LDA models needed to get the inversion status. LDA models have been computed
using the 1000 Genomes Phase 3 data. Real inversion status have been estimated using this data and
invClust. Test scores have been obtained from the same dataset.
}
\keyword{datasets}

